#!/bin/bash

# This file should be executable

##################################################
#                                                #
# Script for downloading YouTube videos          #
# It also checks that 'ytdl' is installed        #
# and up to date!                                #
# By Peter Green                                 #
# Version 0.9.4 (Remember to change below!)      #
# Date of first version; 2022-04-04              #
# Date of last update; 2024-08-03                #
#                                                #
##################################################

# Config...

# Version...
version=0.9.5

# Start config

# How many times should this script run before it checks for updates?
ud_frq=11 # This means the script will run on the number after that which you set here. The counter starts at 0.

# End config.

# Say hi, and declare version number...
echo " 
      
        ##################################################################
        #                                                                #
        #      Welcome to the Greenpete's yt-dlp video download wrapper! #
        #                     Version $version                           #
        #                                                                #
        ##################################################################"

# Move to working dir...
cd ~/Desktop || exit

# Start database incrementation...

# Read the database and put it in a variable...
ud_count=$(cat ~/bin/yt-dlp/db.txt)

# Echo the latest value of the database
echo "
            This script has run $ud_count times since the last check for an update.
            It will check for updates after $ud_frq runs."

# Make the script increment the value in the database by 1 each run...
let "ud_count=ud_count+1"

# Write the new value to the database...
echo "$ud_count" > ~/bin/yt-dlp/db.txt

# End database incrementation...

# Start processing the update counter db...

if [ "$ud_count" -gt $ud_frq ];
    then echo "            We need to update!" && echo "            Checking for updates..." && yt-dlp -U && echo "0" > ~/bin/yt-dlp/db.txt
        else echo "            No need to update yet..."
fi

# End processing the update counter db...

# Start acquiring info' about the desired video to download, ask the user what the URL of the video is...

echo " 
        #############################################################
        #                                                           #
        #  Please enter the URL of the video you want to download - #
        #                                                           #
        #############################################################"

read -r URL #Example - https://youtu.be/S8UNBfatLTo

# Validate user input of the URL variable...
until [[ $URL = https://* ]];
    do echo "            That is not a recognised URL.
            Please try again..." && read -r URL
done

clear

# Ask user if they want to see the download options...
echo "

       #############################################################
       #                                                           #
       #  Would you like to see the video download options? (y/n)  #
       #                                                           #
       #############################################################

"
read -r options

until [ "$options" = y ] || [ "$options" = n ];
    do echo "            That is not a valid answer.
            Your answer must be 'y' or 'n'
            Please try again..." && read -r options
done

clear

if [ "$options" = y ];
    then yt-dlp -F "$URL" && echo "
            Which of the options above would you
            like to choose? (See number at start of
            each line)." && read -r option && clear && yt-dlp -f "$option" "$URL"
        else echo "
       #####################################################
       #                                                   #
       #  Downloading full resolution video with audio...  #
       #                                                   #
       #####################################################" && yt-dlp "$URL"
fi

# End acquiring info' about the desired video to download...

echo "
        ################################################
        #                                              #
        #         Finished downloading.                #
        #                                              #
        #  Please press 'Enter' to close this window.  #
        #                                              #
        ################################################"
read -r

