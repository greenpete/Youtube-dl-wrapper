### News

Youtube-Download_Wrapper now uses yt-dlp, not yt-dl.

### About

Youtube-dl-Wrapper is a Bash script for Linux that makes using yt-dlp (previously youtube-dl) a little easier, but is basically just an exercise in learning [Git](https://git-scm.com/), [GitKraken](https://www.gitkraken.com/), [Intellij](https://www.jetbrains.com/idea/) and [Bash](https://www.gnu.org/software/bash/) for me.<br />
I hope to update and improve this code as well as maybe adding a [Zenity](https://wiki.gnome.org/Projects/Zenity) front end over time.<br />

### Installation

You don't need to install youtube-dl-wrapper, but as part of ensuring and enabling it works as expected, installing helps.

To install, run the installation script in the main directory - ~/yt-dlp/yt-dlp.sh. This will check to see if you have yt-dlp installed and if you don't, it will install it.

### Run

You should find a menu entry under 'Programming'.

Your video will be downloaded to your Desktop.

### Uninstall

If you wish to uninstall this app, then you can do so by running the 'uninstall.sh' script.
This should only uninstall components that were installed when you installed youtube-dl-wrapper, so if you had previously installed yt-dlp yourself, or manually added the 'usr/bin/python' link, you will need to uninstall them yourself manually.

### ::NOTE::

You can have issues if yt-dlp can't find python, to fix this you can create a symlink.

If you get the error "/usr/bin/env: 'python': No such file or directory" then you can run the included script; 'python-error.sh' or open a terminal and execute this code...
```bash{16}
sudo ln -s /usr/bin/python3 /usr/bin/python
```