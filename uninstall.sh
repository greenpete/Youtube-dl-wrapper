#!/bin/bash

# This file should be executable

# Remove youtube-dl
echo "Checking to see if we need to remove yt-dlp"
instytdl="$(cat ./dbytdl.txt)"
if [ "$instytdl" = 1 ];
  then echo "Removing yt-dlp" && sudo rm /usr/local/bin/yt-dlp
    else echo >/dev/null
fi

# Remove python symlink
echo "Checking to see if we need to remove python symlink"
pylnk=$(cat ./dbpyer.txt)
if [ "$pylnk" = 1 ];
  then echo "Removing python sym link" && sudo rm /usr/bin/python
    else echo >/dev/null
fi

# Remove menu entry...
rm "$HOME"/.local/share/applications/yt-dlp.desktop

# Remove YT-DL-W
echo "Removing youtube-dl-wrapper..."
rm -R ~/bin/yt-dlp

echo "Uninstall finished!

Press 'Enter' to close this window..."
read -r
