#!/bin/bash
set -ex
# This file should be executable

# Determine where we are...
pwd=$(pwd)

# Environment check. Checking for youtube-dl. If not found, install.
echo "
            Checking that youtube-dl is installed..."
if [ -f /usr/local/bin/youtube-dl ]
    then echo "0" > ./dbytdl.txt
        else echo "youtube-dl is not present - installing..." && sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl && sudo chmod a+rx /usr/local/bin/youtube-dl && echo "1" > ./dbytdl.txt
fi

# Check for needed directories...
if [ -d ~/bin ];
  then echo >/dev/null
    else mkdir ~/bin
fi

if [ -d ~/bin/YT-DL ];
  then echo >/dev/null
    else mkdir ~/bin/YT-DL
fi

# copy over the files...
cp db.txt ~/bin/YT-DL
cp dbpyer.txt ~/bin/YT-DL
cp dbytdl.txt ~/bin/YT-DL
cp YouTube-DL.svg ~/bin/YT-DL
cp YT-DL.sh ~/bin/YT-DL
cp LICENSE ~/bin/YT-DL
cp README.md ~/bin/YT-DL
cp menu.desktop ~/.local/share/applications
cp uninstall.sh ~/bin/YT-DL
cp python-error.sh ~/bin/YT-DL

# Edit desktop menu file.
# First the Execution path...
sed -i "s|Exec=/path/|Exec=$HOME/bin/YT-DL/YT-DL.sh|" "$HOME"/.local/share/applications/menu.desktop
# Now icon path...
sed -i "s|Icon=/path/|Icon=$HOME/bin/YT-DL/YouTube-DL.svg|" "$HOME"/.local/share/applications/menu.desktop

# Check for the python symlink...
if [ -f /usr/bin/python ];
    then echo >/dev/null
        else echo "There may be problems running youtube-dl itself.
If you get the error message '/usr/bin/env: 'python': No such file or directory',
then you should run the included python-error.sh script to fix it."
fi

# Tell the user we're finished installing...
echo "youtube-dl-wrapper is now installed!

Press 'Enter' to close this window..."
read -r
