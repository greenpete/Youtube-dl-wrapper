#!/bin/bash
#set -ex
# This file should be executable

# Environment check. Checking for youtube-dl. If not found, install.
echo "
            Checking that yt-dlp is installed..."
if [ -f /usr/local/bin/yt-dlp ]
    then echo "0" > ./dbytdl.txt
        else echo "yt-dlp is not present - installing..." && cd /usr/local/bin && sudo wget https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp && sudo chmod a+rx /usr/local/bin/yt-dlp && echo "1" > ./dbytdl.txt
fi

# Check for needed directories...
if [ -d ~/bin ];
  then echo "Looking for you local bin directory - found!"
    else echo "Local bin directory not found - creating" && mkdir ~/bin
fi

if [ -d ~/bin/yt-dlp ];
  then echo "Found you local bin - good!"
    else mkdir ~/bin/yt-dlp
fi

# copy over the files...
cp db.txt ~/bin/yt-dlp
cp dbpyer.txt ~/bin/yt-dlp
cp dbytdl.txt ~/bin/yt-dlp
cp yt-dlp.svg ~/bin/yt-dlp
cp yt-dlp.sh ~/bin/yt-dlp
cp LICENSE ~/bin/yt-dlp
cp README.md ~/bin/yt-dlp
cp yt-dlp.desktop ~/.local/share/applications
cp uninstall.sh ~/bin/yt-dlp
cp python-error.sh ~/bin/yt-dlp

# Edit desktop menu file.
# First the Execution path...
sed -i "s|Exec=/path/|Exec=$HOME/bin/yt-dlp/yt-dlp.sh|" "$HOME"/.local/share/applications/yt-dlp.desktop
# Now icon path...
sed -i "s|Icon=/path/|Icon=$HOME/bin/yt-dlp/yt-dlp.svg|" "$HOME"/.local/share/applications/yt-dlp.desktop

# Check for the python symlink...
if [ -f /usr/bin/python ];
    then echo >/dev/null
        else echo "There may be problems running youtube-dl itself.
If you get the error message '/usr/bin/env: 'python': No such file or directory',
then you should run the included python-error.sh script to fix it."
fi

# Tell the user we're finished installing...
echo "youtube-dl-wrapper is now installed!

Press 'Enter' to close this window..."
read -r
